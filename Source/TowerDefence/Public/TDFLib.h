// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/CurrencyStorageComponent.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TDFLib.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTDFLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure)
	static bool IsAlive(AActor* Actor);

	//Return CurrencyStorage ptr if actor have interface.
	UFUNCTION(BlueprintCallable)
	static UCurrencyStorageComponent* GetCurrencyStorage(AActor* Owner);

	//Call ServerAddCurrency on actor if it have CurrencyStorage.
	UFUNCTION(BlueprintCallable)
	static void AddCurrency(AActor* Recipient, ECurrency Currency, int32 AddValue);

	//Call Server_WithdrawCurrency on actor if it have CurrencyStorage.
	UFUNCTION(BlueprintCallable)
	static void WithdrawCurrency(AActor* Recipient, ECurrency Currency, int32 WithdrawValue);
};
