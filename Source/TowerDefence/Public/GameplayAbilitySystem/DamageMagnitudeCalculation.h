// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "DamageMagnitudeCalculation.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UDamageMagnitudeCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

};
