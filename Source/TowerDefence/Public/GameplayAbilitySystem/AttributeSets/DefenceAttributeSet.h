// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbilitySystem/TDAttributeSet.h"
#include "DefenceAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
 
//ATTRIBUTE_ACCESSORS(UMyHealthSet, Health)



UCLASS()
class TOWERDEFENCE_API UDefenceAttributeSet : public UTDAttributeSet
{
	GENERATED_BODY()

public:
	UDefenceAttributeSet();

	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData DamageReduction;
	ATTRIBUTE_ACCESSORS(UDefenceAttributeSet, DamageReduction)

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
