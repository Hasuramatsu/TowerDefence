// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbilitySystem/TDAttributeSet.h"
#include "HealthAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
 
	//ATTRIBUTE_ACCESSORS(UMyHealthSet, Health)

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthChangeDelegate, float, OldValue, float, NewValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxHealthChangeDelegate, float, OldValue, float, NewValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnDamageTakenDelegate, float, DamageTaken, AActor*, DamageDealer, AActor*, DamageReceiver, bool, bIsKillingBlow);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnHealthRegenerationChangeDelegate, float, OldValue, float, NewValue);
/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UHealthAttributeSet : public UTDAttributeSet
{
	GENERATED_BODY()

public:
	UHealthAttributeSet();

	

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_Health, Category = "Attributes")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UHealthAttributeSet, Health);

	UPROPERTY(BlueprintReadOnly, ReplicatedUsing = OnRep_MaxHealth, Category = "Attributes")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UHealthAttributeSet, MaxHealth);

	UPROPERTY(BlueprintReadOnly, Category = "Attributes") //, ReplicatedUsing = OnRep_HealthRegeneration
	FGameplayAttributeData HealthRegeneration;
	ATTRIBUTE_ACCESSORS(UHealthAttributeSet, HealthRegeneration);

	UPROPERTY(BlueprintReadOnly, Category = "Attributes")
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UHealthAttributeSet, Damage);


	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;


	/************************************
	 *			Delegates				*
	************************************/
	UPROPERTY(BlueprintAssignable)
	FOnHealthChangeDelegate OnHealthChangeDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnMaxHealthChangeDelegate OnMaxHealthChangeDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnDamageTakenDelegate OnDamageTakenDelegate;
	// UPROPERTY(BlueprintAssignable)
	// FOnHealthRegenerationChangeDelegate OnHealthRegenerationChangeDelegate;
	
	
private:


	/************************************
	 *			OnRep Functions			*
	 ************************************/
	
	UFUNCTION()
	virtual void OnRep_Health(const FGameplayAttributeData& OldValue);

	UFUNCTION()
	virtual void OnRep_MaxHealth(const FGameplayAttributeData& OldValue);


	// We don't need it
	// UFUNCTION()
	// virtual void OnRep_HealthRegeneration(const FGameplayAttributeData& OldValue);


	/************************************
	*			Just Functions			*
	************************************/

};
