// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GameplayAbilitySystem/TDAttributeSet.h"
#include "OffenceAttributeSet.generated.h"

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttackRangeChangedDelegate, float, OldValue, float, NewValue);


UCLASS()
class TOWERDEFENCE_API UOffenceAttributeSet : public UTDAttributeSet
{
	GENERATED_BODY()

public:
	UOffenceAttributeSet();

	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData AttackDamage;
	ATTRIBUTE_ACCESSORS(UOffenceAttributeSet, AttackDamage)

	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData AttackSpeed;
	ATTRIBUTE_ACCESSORS(UOffenceAttributeSet, AttackSpeed)

	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData AttackRange;
	ATTRIBUTE_ACCESSORS(UOffenceAttributeSet, AttackRange)

	UPROPERTY(BlueprintAssignable)
	FOnAttackRangeChangedDelegate OnAttackRangeChangedDelegate;

	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData ProjectileSpeedMulti;
	ATTRIBUTE_ACCESSORS(UOffenceAttributeSet, ProjectileSpeedMulti)

	// 
	UPROPERTY(BlueprintReadOnly,  Category = "Attributes")
	FGameplayAttributeData AreaOfEffect;
	ATTRIBUTE_ACCESSORS(UOffenceAttributeSet, AreaOfEffect)


	virtual void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	virtual void PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
};
