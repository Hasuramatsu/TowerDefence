// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "DamageExecuteCalculation.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UDamageExecuteCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
};
