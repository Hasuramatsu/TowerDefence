// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "TDAttributeSet.generated.h"



/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTDAttributeSet : public UAttributeSet
{
	GENERATED_BODY()


protected:

		
	UFUNCTION()
	void AdjustAttributeForMaxChange(FGameplayAttributeData& AffectedAttribute,
												  const FGameplayAttributeData& MaxAttribute, float NewMaxValue, const FGameplayAttribute& AffectedAttributeProperty
);
};
