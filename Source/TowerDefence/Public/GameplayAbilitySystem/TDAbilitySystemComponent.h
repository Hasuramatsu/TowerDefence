// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "TDGameplayAbility.h"
#include "TDAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTDAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
public:

	// Register new abilities for this ASC
	UFUNCTION(BlueprintCallable)
	void RegisterNewAbilities(TArray<TSubclassOf<UTDGameplayAbility>>& NewAbilities);

	UFUNCTION(BlueprintCallable)
	void ApplyEffects(TArray<TSubclassOf<UGameplayEffect>> NewEffects);

	UFUNCTION(BlueprintPure)
	bool GetGameplayAbilitiesWithMatchingTags(const FGameplayTagContainer& GameplayTagContainer, TArray<UTDGameplayAbility*>& ActiveAbilities);
	
protected:

private:
	
	TMap<FString, FGameplayAbilitySpecHandle> OwningAbilities;
};
