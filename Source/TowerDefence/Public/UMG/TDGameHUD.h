// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TDGameHUD.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMenuHide);
/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ATDGameHUD : public AHUD
{
	GENERATED_BODY()


public:
	
	ATDGameHUD();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowMenu();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideMenu();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowGameOver(const bool IsWin);
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnMenuHide OnMenuHide;

protected:

private:
};
