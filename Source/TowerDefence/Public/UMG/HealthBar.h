// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/HealthComponent.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"
#include "HealthBar.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class TOWERDEFENCE_API UHealthBar : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	bool InitHealthBar(AActor* HealthOwner, bool DisplayText);

	
protected:
	UPROPERTY(meta = (BindWidget))
	UProgressBar* HealthBar;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* HealthText;

private:
	UPROPERTY()
	UHealthComponent* OwningHealthComp;

	UFUNCTION()
	void UpdateHealth(float OldValue, float NewValue);
};
