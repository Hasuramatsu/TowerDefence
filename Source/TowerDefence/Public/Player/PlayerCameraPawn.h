// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MapBounds.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/SpringArmComponent.h"
#include "PlayerCameraPawn.generated.h"


USTRUCT(BlueprintType)
struct FCameraSettings
{
	GENERATED_BODY()


	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraSpeed = 700.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera", meta=(ClampMin="0", ClampMax="1"))
	float MinCameraSpeedMulti = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zoom", meta=(ClampMin="0"))
	float BaseZoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zoom", meta=(ClampMin="0"))
	float MinZoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zoom", meta=(ClampMin="0"))
	float MaxZoom;

	// Distance change per activation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zoom", meta=(ClampMin="0"))
	float ZoomChangeDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zoom", meta=(ClampMin="0"))
	float ZoomInterpSpeed;

	// Change camera angle on zoom?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	bool bChangeCameraAngle = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	FRotator BaseAngle = FRotator(-75.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	FRotator MaxAngle = FRotator(-15.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	float ZoomRateForChangingAngle = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	float AngleInterpSpeed = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Angle")
	bool bUseInterp = true;
};

UCLASS()
class TOWERDEFENCE_API APlayerCameraPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerCameraPawn();


	// Stop camera on level bounds?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CameraSettings")
	bool bBounded = true;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Function which move pawn
	void Move(const FVector DeltaLocation);

	UFUNCTION(Server, Unreliable)
	void ServerMove(const FVector DeltaLocation);
	void ServerMove_Implementation(const FVector DeltaLocation);
	bool ServerMove_Validate(const FVector DeltaLocation);

	UFUNCTION(Client, Unreliable)
	void BroadcastMove(const FVector DeltaLocation);
	void BroadcastMove_Implementation(const FVector DeltaLocation);

	// Add value to camera lenght
	void ChangeZoomByValue(float AddValue);

	UFUNCTION(Client, Reliable)
	void InitSettings(const FCameraSettings& NewCameraSettings);

protected:

	UPROPERTY(VisibleAnywhere, Category = Components)
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Components)
	UCameraComponent* CameraComponent;
	
	
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

private:

	UPROPERTY()
	AMapBounds* MapBounds;

	FVector MinMapBounds;
	FVector MaxMapBounds;

	UPROPERTY(Replicated)
	FCameraSettings CameraSettings;

	// Value to interpolate
	UPROPERTY(Replicated)
	float TargetArmLength;

	FTimerHandle ZoomHandle;
	float TimerTick = 0.001;
	float InterpTime  = 1.f;

	void ZoomTick();
};

