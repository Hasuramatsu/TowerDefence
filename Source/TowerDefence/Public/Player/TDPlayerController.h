// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PlayerCameraPawn.h"
//#include "TDPlayerState.h"

#include "TDTypes.h"
#include "GameFramework/PlayerController.h"
#include "Interfaces/CurrencyStorageInterface.h"
#include "Interfaces/TowerStorageInterface.h"
#include "UMG/TDGameHUD.h"
#include "TDPlayerController.generated.h"

class UTowerStorageComponent;
class UTowerData;
class ATDGameHUD;



DECLARE_DELEGATE_OneParam(FIntInput, const int32);
DECLARE_DELEGATE_OneParam(FBoolInput, const bool);

UENUM(BlueprintType)
enum class  EControlState : uint8
{
	None UMETA(DisplayText="None"),
	InMenu UMETA(DisplayText="InMenu"),
	PlacementState UMETA(DisplayText="Placement")
};
/**
 * 
 */

UCLASS()
class TOWERDEFENCE_API ATDPlayerController : public APlayerController, public ITowerStorageInterface, public ICurrencyStorageInterface
{
	GENERATED_BODY()

	
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnReadinessChangeDelegate, ATDPlayerController* , PlayerController, bool, NewValue);

public:
	ATDPlayerController();

	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void OnPossess(APawn* InPawn) override;

	virtual void OnRep_PlayerState() override;

	virtual void InitPlayerState() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual UCurrencyStorageComponent* GetCurrencyStorage_Implementation() override;

	UFUNCTION(BlueprintPure)
	virtual UTowerStorageComponent* GetTowerStorageComponent_Implementation() override;

	
	//Move camera
	UFUNCTION(Client, Unreliable)
	void CameraMoveTick(float DeltaSeconds);

	// Switch variable in player state. 
	UFUNCTION(BlueprintCallable)
	bool SwitchReadyForStart();

	UFUNCTION(BlueprintPure)
	class ATDPlayerState* GetTDPlayerState() const {return TDPlayerState;}

	UFUNCTION(BlueprintPure)
	EControlState GetControlState() const {return ControlState;}

	UFUNCTION(BlueprintPure)
	ATDGameHUD* GetGameHUD() const {return GameHUD;}

	UFUNCTION(BlueprintCallable)
	void SetControlState(EControlState NewState);

	// Spawn dummy for tower client side only.
	UFUNCTION(Client, Unreliable, BlueprintCallable)
	void ClientSpawnDummy(const FString& TowerID);

	// Server func. Spawn and setup tower.
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerSpawnTower(const FTransform& SpawnTransform, AActor* SpawnOwner, const FString& TowerID);

	// Client function. Destroying dummy and call Server SpawnTower func.
	UFUNCTION(Client, Reliable, BlueprintCallable)
	void SpawnTower();


	// Called by Game Mode on Castle Destroy.
	UFUNCTION(Client, Unreliable)
	void OnGameOver(const bool IsWin);

	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTowerStorageComponent* TowerStorageComponent;

	// Camera settings
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "Camera")
	FCameraSettings CameraSettings;

	UPROPERTY(EditAnywhere, Category = "Camera", meta=(ClampMin = "0", ClampMax="0.05"))
	float BorderThreshold = 0.025f;

	UFUNCTION(BlueprintPure)
	float GetCameraMoveMultiplier(float Mouse, float Screen);

	//Input func
	UFUNCTION(Client, Unreliable)
	void CameraFwd(const float Value);

	UFUNCTION(Client, Unreliable)
	void CameraRight(const float Value);

	// Function binded to ESC. Have multiple executions based on statement.
	UFUNCTION(Client, Unreliable)
	void Cancel();

	
	// Function binded to mousewheel. Change zoom of camera.
	UFUNCTION()
	void Zoom(const int32 Value);

	
	// Test variable. Must be stored in array. TODO Make tower "storage" system.
	UPROPERTY(EditDefaultsOnly, Replicated ,BlueprintReadOnly, Category = "Test")
	UTowerData* TowerProperty;
	
private:
	
	//Camera pawn controlled by player;
	UPROPERTY(Replicated)
	APlayerCameraPawn* MyPawn;
	
	UPROPERTY()
	class ATDPlayerState* TDPlayerState;

	UPROPERTY()
	ATDGameHUD* GameHUD;

	// "Shadow" for tower building. Must move in player's action object in future.
	// TODO Player's Action Tower
	UPROPERTY(Replicated)
	class ATowerDummy* SpawnedDummy;
	
	// State need for input choice
	UPROPERTY()
	EControlState ControlState;

	// Control possibility to use "actions(not ready yet)".
	UPROPERTY()
	bool bCanPerformAction = true;
	

	//	Input function for LMB press. DO multiple actions depending on statement.
	UFUNCTION()
	void OnLMBPressed();

	// Input function for LMB release; Do nothing atm.
	UFUNCTION()
	void OnLMBReleased();

	// Function called by HUD when menu hide. Change current control state.
	// TODO Make Interface for UI
	UFUNCTION()
	void OnMenuHide();
};

