// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDPlayerController.h"
#include "Components/CurrencyStorageComponent.h"
#include "GameFramework/PlayerState.h"
#include "Interfaces/CurrencyStorageInterface.h"
#include "TDPlayerState.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnReadinessChangeDelegate, ATDPlayerController*, Player, bool, IsReady);


UCLASS()
class TOWERDEFENCE_API ATDPlayerState : public APlayerState, public ICurrencyStorageInterface
{
	GENERATED_BODY()

public:

	ATDPlayerState();

	UFUNCTION(BlueprintPure)
	bool IsReadyToStart() const {return bReadyToStart;}

	UFUNCTION(BlueprintCallable, Server, Reliable)
	void SetReadyToStart(bool NewReady);

	UFUNCTION(BlueprintCallable)
	bool SwitchReadyToStart();


	//Delegates
	FOnReadinessChangeDelegate OnReadinessChangeDelegate;

	virtual UCurrencyStorageComponent* GetCurrencyStorage_Implementation() override;
protected:

	//Overrides
	
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	UCurrencyStorageComponent* CurrencyStorageComponent;

private:

	UPROPERTY(Replicated) //Using=OnRep_ReadyToStart
	bool bReadyToStart;

	UPROPERTY()
	ATDPlayerController* OwningPlayer;

	UFUNCTION() //Server, Reliable
	void OnRep_ReadyToStart();
};

