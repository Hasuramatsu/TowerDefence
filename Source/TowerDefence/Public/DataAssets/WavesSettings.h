// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WaveInfo.h"
#include "Engine/DataAsset.h"
#include "WavesSettings.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UWavesSettings : public UDataAsset
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	const UWaveInfo* GetWaveInfo(int32 WaveNum);

	UFUNCTION(BlueprintPure)
	bool IsGameInfinite() const {return bInfiniteGame;}

	UFUNCTION(BlueprintPure)
	float GetTimeBetweenSpawns() const {return TimeBetweenSpawns;}

	UFUNCTION(BlueprintPure)
	float GetTimeBetweenWaves() const {return TimeBetweenWaves;}

	UFUNCTION(BlueprintPure)
	int32 GetWavesNum() const {return WavesInfo.Num();}
	

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UWaveInfo*> WavesInfo;
	
	//Will game end after LastWave?
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Wave Settings")
	bool bInfiniteGame;

	// Wait time for spawn next enemy for prevent stack.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Wave Settings")
	float TimeBetweenSpawns;

	// Wait time between wave end and new wave start spawn.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Wave Settings")
	float TimeBetweenWaves;

private:
	
	
};
