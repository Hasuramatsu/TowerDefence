// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/EnemyBase.h"
#include "Engine/DataAsset.h"
#include "EnemyData.generated.h"

class UGameplayEffect;
class UTDGameplayAbility;
/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UEnemyData : public UDataAsset
{
	GENERATED_BODY()

public:

	// Mesh for constructing enemy
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USkeletalMesh* EnemyMesh;

	// AnimBP for mesh
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UClass* AnimBlueprint;

	// Material color for test purpose
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FLinearColor Color;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float BaseMoveSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<UTDGameplayAbility>> StartingAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayEffect>> StartingEffects;

	// Spawn this class if set. Otherwise spawn default
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TSubclassOf<AEnemyBase> ClassToSpawn;
protected:

private:

	
	
};
