// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyData.h"
#include "Engine/DataAsset.h"
#include "WaveInfo.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UWaveInfo : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UEnemyData* EnemyData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 EnemyAmount;
protected:

private:
	
	
};
