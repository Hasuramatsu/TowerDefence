// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDTypes.h"
#include "Engine/DataAsset.h"
#include "TowerData.generated.h"

class UGameplayEffect;
class UTDGameplayAbility;
/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTowerData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString TowerID;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 BuildCost;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
	UStaticMesh* Mesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Visual")
	FLinearColor Color;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Info")
	FDisplayInfo DisplayInfo;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilitySystem")
	TArray<TSubclassOf<UTDGameplayAbility>> StartingAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AbilitySystem")
	TArray<TSubclassOf<UGameplayEffect>> StartingEffects;

protected:
	

private:
	
	
};
