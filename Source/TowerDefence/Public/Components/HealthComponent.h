// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCurrentHealthChange, float, OldValue, float, NewValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnMaxHealthChange, float, OldValue, float, NewValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnKilledDelegate, AActor*, Killer, AActor*, Victim );

// USTRUCT(BlueprintType)
// struct FHealthProperty
// {
// 	GENERATED_BODY()
//
// 	FHealthProperty() {};
// 	
// 	FHealthProperty(float NewMax) : MaxHealth(NewMax)
// 	{
// 		
// 	}
// 	
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
// 	float MaxHealth = 100;
//
// 	
// 	
// };

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UFUNCTION(BlueprintCallable)
	void InitHealth(float NewMaxHealth);
	

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetCurrentHealth() const {return CurrentHealth;}

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetMaxHealth() const {return MaxHealth;}

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetCurrentHealthPercent() const {return MaxHealth / CurrentHealth;}

	// Clamp Value between 0 and MaxHealth
	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SetCurrentHealth(float NewValue);

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void SetMaxHealth(float NewValue);

	// Return damage taken.
	// UFUNCTION(BlueprintCallable)
	// float TakeDamage(const float DamageAmount);

	// Return heal applied
	// UFUNCTION(BlueprintCallable)
	// float TakeHeal(const float HealAmount);

	UFUNCTION(BlueprintPure)
	bool IsAlive() const {return bAlive;}

	// Handles
	UFUNCTION()
	void OnHealthChangeHandle(float OldValue, float NewValue);

	UFUNCTION()
	void OnMaxHealthChangeHandle(float OldValue, float NewValue);

	UFUNCTION()
	void OnDamageTakenHandle(float DamageTaken, AActor* DamageDealer, AActor* DamageReceiver, bool bBIsKillingBlow);

	// Delegates
	UPROPERTY(BlueprintAssignable)
	FOnCurrentHealthChange OnCurrentHealthChangeDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnMaxHealthChange OnMaxHealthChangeDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnDeathDelegate OnDeathDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnKilledDelegate OnKilledDelegate;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	//
	// UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated)
	// FHealthProperty HealthProperty;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Replicated, Category = "Stats")
	float MaxHealth;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
private:

	UPROPERTY()
	bool bAlive;

	UPROPERTY(ReplicatedUsing=OnRep_CurrentHealth)
	float CurrentHealth;

	UFUNCTION()
	void OnRep_CurrentHealth(float OldValue);

	// UFUNCTION()
	// void OnOwnerTakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	//
	// UFUNCTION(NetMulticast, Reliable)
	// void OnCurrentHealthChangeFunc(float OldValue, float NewValue);
};
