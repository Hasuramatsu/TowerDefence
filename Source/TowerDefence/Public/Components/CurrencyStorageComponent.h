// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDTypes.h"
#include "Components/ActorComponent.h"
#include "CurrencyStorageComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnCurrencyValueChanged, ECurrency, CurrencyType, int32, OldValue, int32, NewValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnCurrencyTypeAdded, ECurrency, CurrencyType, int32, Value);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UCurrencyStorageComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCurrencyStorageComponent();


	//Delegates
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnCurrencyValueChanged OnCurrencyValueChanged;

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnCurrencyTypeAdded OnCurrencyTypeAdded;
	

	// Adding new currency type to storage.
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_AddNewCurrencyType(const ECurrency NewCurrency, int32 BaseValue = 0);
	
	//Add some amount of currency to storage.
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_AddCurrency(const ECurrency Currency, int32 Amount);

	//Withdraw some currency.
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void Server_WithdrawCurrency(const ECurrency Currency, int32 Amount);

	UFUNCTION(BlueprintPure)
	bool IsEnoughCurrency(const ECurrency Currency, int32 NeededAmount) const;

	UFUNCTION(BlueprintPure)
	int32 GetCurrencyAmount(const ECurrency Currency) const;

	UFUNCTION(BlueprintPure)
	int32 GetAmount(const ECurrency Currency) const;
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<ECurrency, int32> Currencies;
private:

	
};
