// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DataAssets/TowerData.h"
#include "TowerStorageComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTowerAdded, UTowerData*, TowerData, int32, TowerIndex);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API UTowerStorageComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTowerStorageComponent();

	UPROPERTY(BlueprintAssignable)
	FOnTowerAdded OnTowerAdded;

	// Return TowerData at index.
	UFUNCTION(BlueprintPure)
	UTowerData* GetTower(FString TowerID);

	UFUNCTION(BlueprintPure)
	void GetTowers(TArray<UTowerData*>& OutTowers) const;

	// Add TowerData to array. Return index of new element;
	UFUNCTION(BlueprintCallable)
	FString AddTower(UTowerData* TowerData);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Towers, that player have.
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TArray<UTowerData*> StartingTowers;

private:

	// List of stored towers.
	UPROPERTY()
	TMap<FString, UTowerData*> Towers;
};
