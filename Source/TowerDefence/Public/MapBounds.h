// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "MapBounds.generated.h"

UCLASS()
class TOWERDEFENCE_API AMapBounds : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapBounds();

	UFUNCTION(BlueprintPure)
	FVector GetMinBounds() const;
	
	UFUNCTION(BlueprintPure)
	FVector GetMaxBounds() const;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UBoxComponent* BoxComponent;



};
