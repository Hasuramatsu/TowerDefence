// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Grid.generated.h"

USTRUCT(BlueprintType)
struct FGridProperty
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	int32 GridX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	int32 GridY;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	FVector OriginLocation;
	
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TOWERDEFENCE_API AGrid : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	AGrid();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tile")
	FGridProperty GridProperty;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tile")
	TSubclassOf<ATile> TileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Tile")
	FTileProperty TileProperty;

	UFUNCTION(CallInEditor)
	void DrawDebug();

	UFUNCTION(CallInEditor, BlueprintCallable)
	void SpawnGrid();
	
private:
	
	UPROPERTY()
	TMap<FVector2D, ATile*> TileMap;

		
};
