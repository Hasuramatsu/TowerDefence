// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actors/EnemyBase.h"
#include "DataAssets/WavesSettings.h"
#include "GameFramework/GameMode.h"
#include "Player/TDPlayerController.h"
#include "TDGameMode.generated.h"

UENUM(BlueprintType)
enum class EWaveState : uint8
{
	WaitingForStart UMETA(DisplayName="WaitingForStart"),
	Spawning UMETA(DisplayName="SpawningWave"),
	InProgress UMETA(DisplayName="InProgress")
};


UCLASS()
class TOWERDEFENCE_API ATDGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	
	ATDGameMode();

	// Return array of enemy spawn points.
	UFUNCTION(BlueprintPure)
	void GetEnemySpawnPoints(TArray<AActor*>& Array) const {Array = EnemySpawnPoints;}

	// Start timer for spawning wave.
	UFUNCTION(BlueprintCallable)
	void StartWave();

	// Return current WaveState.
	UFUNCTION(BlueprintPure)
	EWaveState GetWaveState() const {return WaveState;}

	// Return current wave num.
	UFUNCTION(BlueprintPure)
	int32 GetCurrentWaveNum() const {return CurrentWaveNum;}

	UFUNCTION(BlueprintPure)
	bool IsGameOver() const {return bGameOver;}
	
protected:

	// Overrides
	
	virtual void BeginPlay() override;

	virtual void PostLogin(APlayerController* NewPlayer) override;

	virtual void Tick(float DeltaSeconds) override;

	// Blueprint event on castle destroyed;
	UFUNCTION(BlueprintImplementableEvent)
	void Event_OnCastleDestroyed();

	UFUNCTION(BlueprintImplementableEvent)
	void Event_OnWaveEnded();

	//virtual bool ReadyToStartMatch_Implementation() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WaveSettings")
	UWavesSettings* WavesSettings;

	// Array of connected players
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Players")
	TArray<ATDPlayerController*> Players;

	// Map of players readiness.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Players")
	TMap<ATDPlayerController*, bool> PlayersReady;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Debug")
	bool bDebug;

	//Called on castle death or last wave complete.
	//Set bGameOver to true and call OnGameOver for all players;
	UFUNCTION(BlueprintCallable)
	void GameOver(const bool IsWin);

	// End current wave. Check game end conditions.
	UFUNCTION(BlueprintCallable)
	void EndWave();
	
private:

	// Store all enemy spawn points
	UPROPERTY()
	TArray<AActor*> EnemySpawnPoints;

	// EnemyClass to spawn. Set on StartWave();
	UPROPERTY()
	TSubclassOf<AEnemyBase> ClassToSpawn;

	// Display count of alive enemies 
	int32 EnemyAlive;

	// Display current wave state
	UPROPERTY()
	EWaveState WaveState;

	//Current wave
	UPROPERTY()
	int32 CurrentWaveNum = 0;

	// After finishing that wave game will end.
	UPROPERTY()
	int32 LastWaveNum;

	// Display ended game or not.
	bool bGameOver = false;


	UFUNCTION(Server, Reliable, BlueprintCallable)
	void GiveReward(AActor* Killer, AActor* Victim);
	// Timer function for spawn enemy.
	UFUNCTION()
	void SpawnEnemy();
	
	
	// Timer handler for spawning enemy.
	FTimerHandle SpawnTimer;
	// Counter for enemy spawn. Display remaining enemy count for spawn per spawner.
	int32 LoopCount;

	// Binded to enemy death delegate.
	// Count alive enemies, and start timer for new wave if all enemies dead.
	UFUNCTION()
	void OnEnemyDeath();

	// Binded to castle destroyed delegate.
	UFUNCTION()
	void OnCastleDestroyed();

	// Binded to player state delegate.
	// Start match when all players ready.
	UFUNCTION()
	void OnPlayerReady(ATDPlayerController* Player, bool NewReady);
};


