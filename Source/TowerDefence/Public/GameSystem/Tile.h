// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UENUM(BlueprintType)
enum class ETileType : uint8
{
	WallType UMETA(DisplayName = "Wall"),
	GroundType UMETA(DisplayName = "Ground")
};

UENUM(BlueprintType)
enum class EDirectionType : uint8
{
	Up UMETA(DisplayName = "Up"),
	Down UMETA(DisplayName = "Down"),
	Left UMETA(DisplayName = "Left"),
	Right UMETA(DisplayName = "Right")
};

USTRUCT(BlueprintType)
struct FTileProperty
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	float TileSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	ETileType TileType;
	
};

USTRUCT(BlueprintType)
struct FTileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Info")
	FVector2D TileCoordinates;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Info")
	TMap<EDirectionType, class ATile*> Neighbours;
};

UCLASS()
class TOWERDEFENCE_API ATile : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ATile();

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Info")
	FTileInfo TileInfo;

	UFUNCTION()
	void InitTileProperty(const FTileProperty& NewProperty);

	
	UFUNCTION(BlueprintCallable)
	void DrawDebug();
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	USceneComponent* SceneComponent;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Property")
	FTileProperty TileProperty;

};
