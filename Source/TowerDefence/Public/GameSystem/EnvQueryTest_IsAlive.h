// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnvironmentQuery/EnvQueryTest.h"
#include "EnvQueryTest_IsAlive.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UEnvQueryTest_IsAlive : public UEnvQueryTest
{
	GENERATED_BODY()

public:
	UEnvQueryTest_IsAlive();

protected:

	virtual void RunTest(FEnvQueryInstance& QueryInstance) const override;

	
};
