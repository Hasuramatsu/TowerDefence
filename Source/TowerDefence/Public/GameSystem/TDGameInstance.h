// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GameFramework/GameUserSettings.h"
#include "TDGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTDGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;

	UPROPERTY()
	UGameUserSettings* MySettings = nullptr;
};
