// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemInterface.h"
#include "Components/HealthComponent.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/Character.h"
#include "GameplayAbilitySystem/TDAbilitySystemComponent.h"
#include "GameplayAbilitySystem/AttributeSets/DefenceAttributeSet.h"
#include "GameplayAbilitySystem/AttributeSets/HealthAttributeSet.h"
#include "UMG/HealthBar.h"
#include "EnemyBase.generated.h"



class UEnemyData;
USTRUCT(BlueprintType)
struct FCharacterProperties
{
	GENERATED_BODY()

	FCharacterProperties() {};

	FCharacterProperties(float NewHealth, float NewDamage) : Health(NewHealth), Damage(NewDamage) {};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Health;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Damage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<UTDGameplayAbility>> StartingAbilities;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayEffect>> StartingEffects;
};

UCLASS()
class TOWERDEFENCE_API AEnemyBase : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	
	// Sets default values for this character's properties
	AEnemyBase();

	UFUNCTION()
	void InitCharacter(const UEnemyData* Prop);

	UFUNCTION(BlueprintPure)
	UHealthComponent* GetHealthComponent() const {return HealthComponent;}

	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override {return AbilitySystemComponent;}
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Compoments)
	UTDAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Compoments)
	UWidgetComponent* WidgetComp;

	UPROPERTY(BlueprintReadOnly, Category = "AbilitySystem")
	UHealthAttributeSet* HealthAttributes;

	UPROPERTY(BlueprintReadOnly, Category = "AbilitySystem")
	UDefenceAttributeSet* DefenceAttributes;
	
	// Damage that character deal to castle.
	UPROPERTY(BlueprintReadOnly, Category = "Properties")
	float Damage;

	//UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadOnly, Category = "CharacterInfo")
	//FCharacterProperties CharProp;
	
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	// BP Death Event
	UFUNCTION(BlueprintImplementableEvent)
	void OnDeathEvent();
private:
	
	UFUNCTION()
	void OnDeath();
	

	bool bInitialized;
};
