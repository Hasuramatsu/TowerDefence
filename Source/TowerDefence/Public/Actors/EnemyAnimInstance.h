// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnemyBase.h"
#include "Animation/AnimInstance.h"
#include "EnemyAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure)
	AEnemyBase* TryGetOwnerEnemy() {return OwnEnemy;}
	
	UFUNCTION(BlueprintPure, meta=(BlueprintThreadSafe))
	bool IsAlive() const {return bIsAlive;}

	UFUNCTION(BlueprintPure, meta=(BlueprintThreadSafe))
	static UAnimSequence* GetRandomSequenceFromArray(TArray<UAnimSequence*>& Array);

protected:

	virtual void NativeBeginPlay() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnDead_BP();
private:

	UPROPERTY()
	AEnemyBase* OwnEnemy;

	UPROPERTY()
	bool bIsAlive = true;

	UFUNCTION(Server, Reliable)
	void OnDead();
	
};
