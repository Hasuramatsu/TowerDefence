// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/HealthComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Castle.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCastleDestroyed);

UCLASS()
class TOWERDEFENCE_API ACastle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACastle();

	UFUNCTION(BlueprintPure)
	UHealthComponent* GetHealthComponent() const {return HealthComponent;}

	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnCastleDestroyed OnCastleDestroyed;

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	USphereComponent* CollisionSphere;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* Mesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UHealthComponent* HealthComponent;


	UFUNCTION()
	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnDeath();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDeathEvent();
private:

	
};
