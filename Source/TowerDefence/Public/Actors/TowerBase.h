// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDTypes.h"
#include "GameFramework/Pawn.h"
#include "GameplayAbilitySystem/TDAbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "GameplayAbilitySystem/AttributeSets/OffenceAttributeSet.h"
#include "Interfaces/CurrencyStorageInterface.h"
#include "TowerBase.generated.h"

class ATDPlayerController;
class UTowerData;
UCLASS()
class TOWERDEFENCE_API ATowerBase : public APawn, public IAbilitySystemInterface, public ICurrencyStorageInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATowerBase();

	UFUNCTION(BlueprintCallable, NetMulticast, Reliable)
	void InitTower(const UTowerData* Property, ATDPlayerController* NewPlayerController);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
	UOffenceAttributeSet* GetOffenceAttributeSet() {return OffenceAttributes;}

	UFUNCTION(BlueprintPure)
	ATDPlayerController* GetPlayerController() const;

	virtual UCurrencyStorageComponent* GetCurrencyStorage_Implementation() override;	
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override {return AbilitySystemComponent;}
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UTDAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Abilities")
	UOffenceAttributeSet* OffenceAttributes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	FDisplayInfo DisplayInfo;

	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


private:	

	// Was InitTower func called?
	UPROPERTY(Replicated)
	bool bInitialized;

	UPROPERTY(Replicated)
	ATDPlayerController* PlayerController;


};
