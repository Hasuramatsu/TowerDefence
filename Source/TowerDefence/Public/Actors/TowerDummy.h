// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Player/TDPlayerController.h"
#include "TowerDummy.generated.h"

UCLASS()
class TOWERDEFENCE_API ATowerDummy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATowerDummy();

	UFUNCTION(BlueprintCallable)
	void SetOwningPlayer(ATDPlayerController* Player) {OwningPlayer = Player;}

	UFUNCTION(BlueprintPure)
	bool IsValidLocation() const {return bValidLocation;}

	UFUNCTION(BlueprintCallable)
	void InitDummy(UStaticMesh* Mesh, FString NewTowerID, ATDPlayerController* Player);

	UFUNCTION(BlueprintPure)
	FString GetTowerID() const {return TowerID;}
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	USceneComponent* SceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	// Num of cols and rows of "tiles" generated for trace;
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	int32 GridSize;

	// Size of one side of object
	UPROPERTY(EditDefaultsOnly, Category = "Config")
	float ObjectSize;


	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	bool bValidLocation;
	
	UPROPERTY()
	UMaterialInstanceDynamic* DynamicMaterial;

	UPROPERTY()
	UMaterialInterface* Material;

	UPROPERTY()
	class ATDPlayerController* OwningPlayer;

	UPROPERTY()
	FString TowerID;

	// Do multiple traces to check place is valid
	UFUNCTION()
	bool IsValidPlace();
};
