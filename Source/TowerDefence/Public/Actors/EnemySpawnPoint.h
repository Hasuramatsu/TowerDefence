// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ArrowComponent.h"
#include "GameFramework/Actor.h"
#include "EnemySpawnPoint.generated.h"

class UCapsuleComponent;
UCLASS()
class TOWERDEFENCE_API AEnemySpawnPoint : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemySpawnPoint();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UBillboardComponent* Billboard;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Components)
	UArrowComponent* ArrowComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
