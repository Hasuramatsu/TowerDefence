// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TowerController.generated.h"

class ATowerBase;
/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API ATowerController : public AAIController
{
	GENERATED_BODY()

public:
	ATowerController();

	UFUNCTION()
	void OnAttackRangeChangedHandle(float OldValue, float NewValue);
	
	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UBehaviorTree* BehaviorTree;

private:

	UPROPERTY()
	ATowerBase* PossesedTower;
	
};
