// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TDTypes.generated.h"

class UGameplayEffect;
class UTDGameplayAbility;


UENUM(BlueprintType)
enum class ECurrency : uint8
{
	Gold UMETA(DisplayText="Gold")
};

USTRUCT(BlueprintType)
struct FDisplayInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText DisplayName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FText Description;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UTexture2D* Thumbnail;
	
	
};

// USTRUCT(BlueprintType)
// struct FTowerProperty : public FTableRowBase
// {
// 	GENERATED_BODY()
//
// 	FTowerProperty()
// 	{
// 		//TODO Set static mesh
// 	}
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
// 	UStaticMesh* StaticMesh;
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
// 	FDisplayInfo DisplayInfo;
//
// 	
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	TArray<TSubclassOf<UTDGameplayAbility>> StartingAbilities;
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	TArray<TSubclassOf<UGameplayEffect>> StartingEffects;
// 	
// };

// USTRUCT(BlueprintType)
// struct FCurrency 
// {
// 	GENERATED_BODY()
//
// 	FCurrency(){};
// 	
// 	FCurrency(const int32 NewValue) : Value(NewValue){}
// 	
// 	FName Title;
//
// 	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
// 	int32 Value;
//
// 	FCurrency& operator+=(const FCurrency& Y)
// 	{
// 		Value += Y.Value;
// 		return *this;
// 	}
//
// 	friend FCurrency operator+(FCurrency X, const int32 Y)
// 	{
// 		X.Value += Y;
// 		return X;
// 	}
//
// 	FCurrency& operator-=(const FCurrency& Y)
// 	{
// 		Value -= Y.Value;
// 		return *this;
// 	}
//
// 	friend FCurrency operator-(FCurrency X, const int32 Y)
// 	{
// 		X.Value -= Y;
// 		return X;
// 	}
//
//
// 	bool operator < (const FCurrency& Y) {return Value < Y.Value;}
// 	bool operator > (const FCurrency& Y) {return Value > Y.Value;}
// 	bool operator <= (const FCurrency& Y) {return Value <= Y.Value;}
// 	bool operator >= (const FCurrency& Y) {return Value >= Y.Value;}
//
// 	bool operator ==(const FCurrency& Y)
// 	{
// 		return Title == Y.Title && Value == Y.Value;
// 	}
//
// 	bool operator != (const FCurrency& Y) const
// 	{
// 		return false;//!(*this == Y);
// 	}
// 	
// };


/**
 * 
 */
UCLASS()
class TOWERDEFENCE_API UTDTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
