// Fill out your copyright notice in the Description page of Project Settings.


#include "TDFLib.h"

#include "Components/HealthComponent.h"
#include "Interfaces/CurrencyStorageInterface.h"

bool UTDFLib::IsAlive(AActor* Actor)
{

	UHealthComponent* HealthComponent = Cast<UHealthComponent>(Actor->GetComponentByClass(UHealthComponent::StaticClass()));
	if(HealthComponent)
	{
		return HealthComponent->IsAlive();
	}
	
	return false;
}

UCurrencyStorageComponent* UTDFLib::GetCurrencyStorage(AActor* Owner)
{
	if(!Cast<ICurrencyStorageInterface>(Owner)) return nullptr;

	return ICurrencyStorageInterface::Execute_GetCurrencyStorage(Owner);
}

void UTDFLib::AddCurrency(AActor* Recipient, ECurrency Currency, int32 AddValue)
{
	if(UCurrencyStorageComponent* Storage = GetCurrencyStorage(Recipient))
	{
		Storage->Server_AddCurrency(Currency, AddValue);
	}
}

void UTDFLib::WithdrawCurrency(AActor* Recipient, ECurrency Currency, int32 WithdrawValue)
{
	if(UCurrencyStorageComponent* Storage = GetCurrencyStorage(Recipient))
	{
		Storage->Server_WithdrawCurrency(Currency, WithdrawValue);
	}
}
