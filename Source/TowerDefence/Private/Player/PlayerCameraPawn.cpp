// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/PlayerCameraPawn.h"

#include "MapBounds.h"
#include "Kismet/GameplayStatics.h"
#include "Math/Vector.h"
#include "Net/UnrealNetwork.h"

// Sets default values
APlayerCameraPawn::APlayerCameraPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
	SpringArmComponent->SetRelativeRotation(FRotator(-75.f, 0.f, 0.f));
	SpringArmComponent->bDoCollisionTest = false;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	bReplicates = true;
	bOnlyRelevantToOwner = true;
	
}

// Called when the game starts or when spawned
void APlayerCameraPawn::BeginPlay()
{
	Super::BeginPlay();

	SetActorRotation(FRotator(0.f, -90.f, 0.f));

	MapBounds = Cast<AMapBounds>(UGameplayStatics::GetActorOfClass(GetWorld(), AMapBounds::StaticClass()));
	if(MapBounds)
	{
		MinMapBounds = MapBounds->GetMinBounds();
		MaxMapBounds = MapBounds->GetMaxBounds();
	}
}

void APlayerCameraPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerCameraPawn, TargetArmLength);
	DOREPLIFETIME(APlayerCameraPawn, CameraSettings);
}


void APlayerCameraPawn::InitSettings_Implementation(const FCameraSettings& NewCameraSettings)
{
	CameraSettings = NewCameraSettings;

	SpringArmComponent->TargetArmLength = CameraSettings.BaseZoom;
	TargetArmLength = CameraSettings.BaseZoom;
}



// Called every frame
void APlayerCameraPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APlayerCameraPawn::Move(const FVector DeltaLocation)
{
	ServerMove(DeltaLocation);
}

void APlayerCameraPawn::ServerMove_Implementation(const FVector DeltaLocation)
{
	if(GetLocalRole() == ROLE_Authority)
	{
		BroadcastMove(DeltaLocation);
	}
}

bool APlayerCameraPawn::ServerMove_Validate(const FVector DeltaLocation)
{
	return true;
}

void APlayerCameraPawn::BroadcastMove_Implementation(const FVector DeltaLocation)
{
	FVector NewDelta = DeltaLocation;
	FVector Loc = GetActorLocation();
	
	if(MapBounds || bBounded)
	{
		
		
		if((FMath::IsNearlyEqual(Loc.X, MinMapBounds.X) && NewDelta.X < 0 ) || (FMath::IsNearlyEqual(Loc.X, MaxMapBounds.X) && NewDelta.X > 0))
		{
			NewDelta.X = 0.f;
		}

		if((FMath::IsNearlyEqual(Loc.Y, MinMapBounds.Y) && NewDelta.Y < 0) || (FMath::IsNearlyEqual(Loc.Y, MaxMapBounds.Y) && NewDelta.Y > 0))
		{
			NewDelta.Y = 0.f;
		}

		if(FMath::IsNearlyEqual(NewDelta.X, 0.f) && FMath::IsNearlyEqual(NewDelta.Y, 0.f))
		{
			return;
		}
	}
	else if(bBounded)
	{
		// TODO Remove Debug message
		GEngine->AddOnScreenDebugMessage(-1, 0.f, FColor::Red, TEXT("APlayerCameraPawn::Move -- No bounds detected!"));
	}

	AddActorWorldOffset(NewDelta);
	Loc = GetActorLocation();

	if(Loc.X > MaxMapBounds.X || Loc.X < MinMapBounds.X || Loc.Y > MaxMapBounds.Y || Loc.Y < MinMapBounds.Y)
	{
		const FVector NewLocation = ClampVector(Loc, MinMapBounds, MaxMapBounds);
		SetActorLocation(NewLocation);
	}
}


void APlayerCameraPawn::ChangeZoomByValue(float AddValue)
{
	// if(SpringArmComponent->TargetArmLength <= CameraSettings.MinZoom || SpringArmComponent->TargetArmLength >= CameraSettings.MaxZoom)
	// {
	// 	TargetArmLength = SpringArmComponent->TargetArmLength;
	// 	return;
	// }
	
	TargetArmLength = FMath::Clamp(TargetArmLength + AddValue, CameraSettings.MinZoom, CameraSettings.MaxZoom);
		
	GetWorld()->GetTimerManager().SetTimer(ZoomHandle, this, &APlayerCameraPawn::ZoomTick, TimerTick, true, 0.f);
}

void APlayerCameraPawn::ZoomTick()
{
	float& ArmLength = SpringArmComponent->TargetArmLength;
	
	ArmLength = FMath::FInterpTo(ArmLength, TargetArmLength, TimerTick, CameraSettings.ZoomInterpSpeed);
	
	//float NewValue = FMath::InterpEaseInOut<float>(ArmLength, TargetArmLength, TimerTick, CameraSettings.ZoomInterpSpeed);
	//FString String = FString::Printf(TEXT("NewValue = %f"), NewValue);
	//GEngine->AddOnScreenDebugMessage(-1, TimerTick, FColor::Yellow, String);

	if(CameraSettings.bChangeCameraAngle && ArmLength / CameraSettings.BaseZoom <= CameraSettings.ZoomRateForChangingAngle)
	{
		if(CameraSettings.bUseInterp)
		{
			FRotator TargetAngle = ArmLength > TargetArmLength ? CameraSettings.MaxAngle : CameraSettings.BaseAngle;
			SpringArmComponent->SetRelativeRotation(FMath::RInterpTo(SpringArmComponent->GetRelativeRotation(), TargetAngle, TimerTick, CameraSettings.AngleInterpSpeed));
			
		}
		else
		{
			float NewAngle = FMath::GetMappedRangeValueClamped(FVector2D(CameraSettings.ZoomRateForChangingAngle, 0.1f), FVector2D(CameraSettings.BaseAngle.Pitch, CameraSettings.MaxAngle.Pitch), ArmLength / CameraSettings.BaseZoom);

			SpringArmComponent->SetRelativeRotation(FRotator(NewAngle, 0.f, 0.f));
		}
		
	}
	else if(SpringArmComponent->GetRelativeRotation() != CameraSettings.BaseAngle)
	{
		SpringArmComponent->SetRelativeRotation(CameraSettings.BaseAngle);
	}
	
	if(ArmLength >= CameraSettings.MaxZoom || ArmLength <= CameraSettings.MinZoom)
	{
		ArmLength = FMath::Clamp(ArmLength, CameraSettings.MinZoom, CameraSettings.MaxZoom);
		TargetArmLength = ArmLength;

		GetWorld()->GetTimerManager().ClearTimer(ZoomHandle);
	}
}

