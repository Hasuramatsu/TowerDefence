// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TDPlayerState.h"

#include "Net/UnrealNetwork.h"

ATDPlayerState::ATDPlayerState()
{

	CurrencyStorageComponent = CreateDefaultSubobject<UCurrencyStorageComponent>(TEXT("CurrencuStorage"));
	
	bReplicates = true;
	bReadyToStart = false;
	
}

void ATDPlayerState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDPlayerState, bReadyToStart);
}


UCurrencyStorageComponent* ATDPlayerState::GetCurrencyStorage_Implementation()
{
	return CurrencyStorageComponent;
}



void ATDPlayerState::SetReadyToStart_Implementation(bool NewReady)
{
	bReadyToStart = NewReady;

	ATDPlayerController* PlayerController = Cast<ATDPlayerController>(GetOwner());
	if(PlayerController && GetLocalRole() == ROLE_Authority)
	{
		OnReadinessChangeDelegate.Broadcast(PlayerController, bReadyToStart);
	}
	//OnRep_ReadyToStart();

}

bool ATDPlayerState::SwitchReadyToStart()
{
	SetReadyToStart(!bReadyToStart);

	return bReadyToStart;
}



void ATDPlayerState::OnRep_ReadyToStart() //_Implementation
{


	
}
