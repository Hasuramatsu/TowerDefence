// Fill out your copyright notice in the Description page of Project Settings.


#include "Player/TDPlayerController.h"

#include "TDFLib.h"
#include "Actors/TowerBase.h"
#include "Actors/TowerDummy.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Player/TDPlayerState.h"
#include "Components/CurrencyStorageComponent.h"
#include "Actors/TowerDummy.h"
#include "Components/TowerStorageComponent.h"
#include "UMG/TDGameHUD.h"

class ATDPlayerState;

ATDPlayerController::ATDPlayerController()
{
	//bIsReadyForStart = false;
	bReplicates = true;

	ControlState = EControlState::None;

	TowerStorageComponent = CreateDefaultSubobject<UTowerStorageComponent>(TEXT("TowerStorageComponent"));
	
}

void ATDPlayerController::BeginPlay()
{
	Super::BeginPlay();

	GameHUD = Cast<ATDGameHUD>(GetHUD());
}


void ATDPlayerController::InitPlayerState()
{
	Super::InitPlayerState();

	TDPlayerState = GetPlayerState<ATDPlayerState>();
}


void ATDPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATDPlayerController, MyPawn);
	DOREPLIFETIME(ATDPlayerController, CameraSettings);
	DOREPLIFETIME(ATDPlayerController, SpawnedDummy);
	DOREPLIFETIME(ATDPlayerController, TowerProperty);
}


void ATDPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis(TEXT("CameraFwd"), this, &ATDPlayerController::CameraFwd);
	InputComponent->BindAxis(TEXT("CameraRight"), this, &ATDPlayerController::CameraRight);

	FInputActionBinding& Cancel = InputComponent->BindAction(TEXT("Cancel"), IE_Pressed, this, &ATDPlayerController::Cancel);
	Cancel.bExecuteWhenPaused = true;
	
	InputComponent->BindAction<FIntInput>(TEXT("ZoomOut"), IE_Pressed, this, &ATDPlayerController::Zoom, 1);
	InputComponent->BindAction<FIntInput>(TEXT("ZoomIn"), IE_Pressed, this, &ATDPlayerController::Zoom, -1);
	InputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ATDPlayerController::OnLMBPressed);
	InputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ATDPlayerController::OnLMBReleased);
}

void ATDPlayerController::OnLMBPressed()
{
	if(!bCanPerformAction) return;
	
	switch (ControlState)
	{
	case EControlState::None:
		
		break;
	case EControlState::InMenu:
		
		break;
	case EControlState::PlacementState:
		if(SpawnedDummy && SpawnedDummy->IsValidLocation())
		{
			SpawnTower();

		}
		
		break;
	}
}

void ATDPlayerController::OnLMBReleased()
{
	if(!bCanPerformAction) return;
	
	switch (ControlState)
	{
	case EControlState::None:
		
		break;
	case EControlState::InMenu:
		
		break;
	case EControlState::PlacementState:
		
		break;
	}
}

void ATDPlayerController::OnMenuHide()
{
	if(GameHUD)
	{
		GameHUD->OnMenuHide.RemoveDynamic(this, &ATDPlayerController::OnMenuHide);

		SetControlState(EControlState::None);
	}
	
}


void ATDPlayerController::CameraFwd_Implementation(const float Value)
{
	if(MyPawn && !FMath::IsNearlyZero(Value))
	{
		const FVector MovementVector = FVector(Value * GetWorld()->GetDeltaSeconds() * CameraSettings.CameraSpeed, 0.f, 0.f);
		MyPawn->ServerMove(MovementVector);
	}
}

void ATDPlayerController::CameraRight_Implementation(const float Value)
{
	if(MyPawn && !FMath::IsNearlyZero(Value))
	{
		const FVector MovementVector = FVector(0.f, Value * GetWorld()->GetDeltaSeconds() * CameraSettings.CameraSpeed, 0.f);
		MyPawn->ServerMove(MovementVector);
	}
}

void ATDPlayerController::Cancel_Implementation()
{
	switch (ControlState)
	{
	case EControlState::None:
		if(GameHUD)
		{
			GameHUD->ShowMenu();

			GameHUD->OnMenuHide.AddDynamic(this, &ATDPlayerController::OnMenuHide);

			ControlState = EControlState::InMenu;
		}
		
		break;
	case EControlState::InMenu:

		if(GameHUD)
		{
			GameHUD->HideMenu();
		}
		
		break;
	case EControlState::PlacementState:
		
		ControlState = EControlState::None;
		if(SpawnedDummy)
		{
			SpawnedDummy->Destroy();
		}
		
		break;
	}

}

void ATDPlayerController::Zoom(const int32 Value)
{
	if(MyPawn)
	{
		MyPawn->ChangeZoomByValue(Value * CameraSettings.ZoomChangeDistance);
	}
}


void ATDPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	MyPawn = Cast<APlayerCameraPawn>(InPawn);

	if(MyPawn)
	{
		MyPawn->InitSettings(CameraSettings);
	}
	
}

void ATDPlayerController::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	TDPlayerState = GetPlayerState<ATDPlayerState>();
}




void ATDPlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Check cursor coords and move pawn
	CameraMoveTick(DeltaSeconds);
}

UCurrencyStorageComponent* ATDPlayerController::GetCurrencyStorage_Implementation()
{

	return Cast<ICurrencyStorageInterface>(TDPlayerState) ? Execute_GetCurrencyStorage(TDPlayerState) : nullptr;
}


void ATDPlayerController::CameraMoveTick_Implementation(float DeltaSeconds)
{
	if(MyPawn)
	{
		float MouseX;
		float MouseY;
		FVector2D MousePosition;
		
		if(GetMousePosition(MouseX, MouseY))
		{
			int32 ScreenX;
			int32 ScreenY;
			GetViewportSize(ScreenX, ScreenY);

			float XOffset = GetCameraMoveMultiplier(MouseX, ScreenX) * CameraSettings.CameraSpeed * DeltaSeconds;
			float YOffset = GetCameraMoveMultiplier(MouseY, ScreenY) * CameraSettings.CameraSpeed * DeltaSeconds;
			MyPawn->ServerMove(FVector(XOffset, YOffset, 0.f));
		}
		
	}
}





bool ATDPlayerController::SwitchReadyForStart()
{

	if(TDPlayerState)
	{
		return TDPlayerState->SwitchReadyToStart();
	}

	return false;
}

void ATDPlayerController::SetControlState(EControlState NewState)
{
	ControlState = NewState;
}

void ATDPlayerController::SpawnTower_Implementation()
{
	const FTransform SpawnTransform = SpawnedDummy->GetTransform();
	const FString TowerID = SpawnedDummy->GetTowerID();

	const UTowerData* TowerData = TowerStorageComponent->GetTower(TowerID);
	UCurrencyStorageComponent* CurrencyStorage = Execute_GetCurrencyStorage(this);
	
	if(!CurrencyStorage->IsEnoughCurrency(ECurrency::Gold, TowerData->BuildCost))
	{
		// TODO Make reaction on not enough currency
		const int32 Value = TowerData->BuildCost - CurrencyStorage->GetCurrencyAmount(ECurrency::Gold);
		const FString String = FString::Printf(TEXT("Not enough %i gold"), Value);
		GEngine->AddOnScreenDebugMessage(-1, 3.f, FColor::Red, String);
		
		return;
	}

	CurrencyStorage->Server_WithdrawCurrency(ECurrency::Gold, TowerData->BuildCost);
	
	SpawnedDummy->Destroy();

	SetControlState(EControlState::None);
	
	ServerSpawnTower(SpawnTransform, this, TowerID);

}


void ATDPlayerController::ClientSpawnDummy_Implementation(const FString& TowerID)
{
	if(ControlState != EControlState::None || !bCanPerformAction)
	{
		return;
	}
	FTransform SpawnTransform;
	
	FHitResult HitResult;
	GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, false, HitResult);

	if(HitResult.IsValidBlockingHit())
	{
		SpawnTransform.SetLocation(HitResult.Location);
	}
	
	SpawnedDummy = GetWorld()->SpawnActorDeferred<ATowerDummy>(ATowerDummy::StaticClass(), SpawnTransform, this);
	if(SpawnedDummy)
	{
		UStaticMesh* Mesh = TowerStorageComponent->GetTower(TowerID)->Mesh;
		if(Mesh)
		{
			SpawnedDummy->InitDummy(Mesh, TowerID, this);
			SetControlState(EControlState::PlacementState);
		}

		SpawnedDummy->FinishSpawning(SpawnTransform);

	}
}

void ATDPlayerController::ServerSpawnTower_Implementation(const FTransform& SpawnTransform, AActor* SpawnOwner,const FString& TowerID)
{
	ATowerBase* Tower = GetWorld()->SpawnActorDeferred<ATowerBase>(ATowerBase::StaticClass(), SpawnTransform, SpawnOwner);
	if(Tower)
	{
		Tower->InitTower(TowerStorageComponent->GetTower(TowerID), this);
		
		Tower->FinishSpawning(SpawnTransform);
	}
}


void ATDPlayerController::OnGameOver_Implementation(const bool IsWin)
{

	bCanPerformAction = false;

	if(ControlState == EControlState::PlacementState) // If we in menu we dont need change it
	{
		SetControlState(EControlState::None);
	}

	if(SpawnedDummy)
	{
		SpawnedDummy->Destroy();
	}
	
	GameHUD->ShowGameOver(IsWin);
}

UTowerStorageComponent* ATDPlayerController::GetTowerStorageComponent_Implementation()
{
	return TowerStorageComponent;
}

float ATDPlayerController::GetCameraMoveMultiplier(float Mouse, float Screen)
{
	float Delta = Mouse / Screen;
	if(Delta >= 1-BorderThreshold)
	{
		const FVector2D InputRange = FVector2D(1-BorderThreshold, 1);
		const FVector2D OutputRange = FVector2D(CameraSettings.MinCameraSpeedMulti, 1);
		return FMath::GetMappedRangeValueClamped(InputRange, OutputRange, Delta);
	}
	else if(Delta <= 0+BorderThreshold)
	{
		const FVector2D InputRange = FVector2D(0, 0+BorderThreshold);
		const FVector2D OutputRange = FVector2D(-1, -CameraSettings.MinCameraSpeedMulti);
		return FMath::GetMappedRangeValueClamped(InputRange, OutputRange, Delta);
	}

	return 0;
}


// void ATDPlayerController::OnRep_ReadyForStart()
// {
// 	
// }

