// Fill out your copyright notice in the Description page of Project Settings.


#include "UMG/HealthBar.h"

#include "Components/HealthComponent.h"


bool UHealthBar::InitHealthBar(AActor* HealthOwner, bool DisplayText)
{
	
	if(!DisplayText)
	{
		HealthText->Visibility = ESlateVisibility::Hidden;
	}
	
	if(HealthOwner)
	{
		OwningHealthComp = Cast<UHealthComponent>(HealthOwner->FindComponentByClass(UHealthComponent::StaticClass()));
		if(OwningHealthComp)
		{
			OwningHealthComp->OnCurrentHealthChangeDelegate.AddDynamic(this, &UHealthBar::UpdateHealth);

			UpdateHealth(OwningHealthComp->GetCurrentHealth(), OwningHealthComp->GetCurrentHealth());
			
			return true;
		}
	}

	
	return false;
}

void UHealthBar::UpdateHealth(float OldValue, float NewValue)
{
	HealthBar->SetPercent(NewValue / OwningHealthComp->GetMaxHealth());

	// TODO

	FString String = FString::Printf(TEXT("%i / %i"), (int32)NewValue, (int32)OwningHealthComp->GetMaxHealth());
	HealthText->SetText(FText::FromString(String));
}
