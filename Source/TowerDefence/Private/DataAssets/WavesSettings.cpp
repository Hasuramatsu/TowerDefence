// Fill out your copyright notice in the Description page of Project Settings.


#include "DataAssets/WavesSettings.h"

const UWaveInfo* UWavesSettings::GetWaveInfo(int32 WaveNum)
{
	if(WaveNum > WavesInfo.Num() || WaveNum <= 0) return nullptr;

	return WavesInfo[WaveNum-1];
}
