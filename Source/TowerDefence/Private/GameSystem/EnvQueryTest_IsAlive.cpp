// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSystem/EnvQueryTest_IsAlive.h"

#include "TDFLib.h"

UEnvQueryTest_IsAlive::UEnvQueryTest_IsAlive()
{
	Cost = EEnvTestCost::Low;
}

void UEnvQueryTest_IsAlive::RunTest(FEnvQueryInstance& QueryInstance) const
{
	UObject* QueryOwner = QueryInstance.Owner.Get();
	if(!QueryOwner)
	{
		return;
	}

	BoolValue.BindData(QueryOwner, QueryInstance.QueryID);
	bool bWantsValid = BoolValue.GetValue();
	
	for(FEnvQueryInstance::ItemIterator It(this, QueryInstance); It; ++It)
	{
		AActor* ItemActor = GetItemActor(QueryInstance, It.GetIndex());
		if(ItemActor)
		{
			It.SetScore(TestPurpose, FilterType, UTDFLib::IsAlive(ItemActor), bWantsValid);
		}
		else
		{
			It.ForceItemState(EEnvItemStatus::Failed);
		}
	}
}
