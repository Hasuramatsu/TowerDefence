// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSystem/TDGameInstance.h"

void UTDGameInstance::Init()
{
	Super::Init();

	MySettings = UGameUserSettings::GetGameUserSettings();

	if(MySettings)
	{
		MySettings->RunHardwareBenchmark();
		MySettings->ApplyHardwareBenchmarkResults();
	}
}
