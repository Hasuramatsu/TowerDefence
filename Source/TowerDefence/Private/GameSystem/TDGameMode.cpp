// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSystem/TDGameMode.h"

#include "TDFLib.h"
#include "Actors/Castle.h"
#include "Player/TDPlayerState.h"
#include "Actors/EnemySpawnPoint.h"
#include "DataAssets/EnemyData.h"
#include "Kismet/GameplayStatics.h"


ATDGameMode::ATDGameMode()
{
	EnemyAlive = 0;
	LastWaveNum = 5;
	LoopCount = 0;
	LastWaveNum = 0;
	WaveState = EWaveState::WaitingForStart;

	bDebug = false;
}


void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Store enemy spawners;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemySpawnPoint::StaticClass(), EnemySpawnPoints);

	// Bind to castle destroyed to track game ended.
	if(ACastle* Castle = Cast<ACastle>(UGameplayStatics::GetActorOfClass(GetWorld(), ACastle::StaticClass())))
	{
		Castle->OnCastleDestroyed.AddDynamic(this, &ATDGameMode::OnCastleDestroyed);
	}


	if(WavesSettings)
	{
		LastWaveNum = WavesSettings->GetWavesNum();
	}
}

void ATDGameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	ATDPlayerController* NewPC = Cast<ATDPlayerController>(NewPlayer);
	if(NewPC && NewPC->GetTDPlayerState())
	{
		Players.Add(NewPC);
		PlayersReady.Add(NewPC, NewPC->GetTDPlayerState()->IsReadyToStart());

		//Bind check that all players ready for start match on players click "Ready".
		NewPC->GetTDPlayerState()->OnReadinessChangeDelegate.AddDynamic(this, &ATDGameMode::ATDGameMode::OnPlayerReady);
	}
	
}

void ATDGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(bDebug)
	{
		const FString DebugString = FString::Printf(TEXT("Enemy Alive: %i"), EnemyAlive);
		GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Red, DebugString);
	}
}

// Called by OnPlayerReady() and by timer in OnEnemyDeath()
void ATDGameMode::StartWave()
{
	// Increasing Current Wave count;
	++CurrentWaveNum;

	if(!WavesSettings) return; // TODO Must be called early. Remove.
	if(!WavesSettings->GetWaveInfo(CurrentWaveNum)) return;
	if(!WavesSettings->GetWaveInfo(CurrentWaveNum)->EnemyData) return;

	// Find enemy class in enemy info.
	ClassToSpawn = WavesSettings->GetWaveInfo(CurrentWaveNum)->EnemyData->ClassToSpawn;

	// If class not found use default.
	if(!ClassToSpawn)
	{
		ClassToSpawn = AEnemyBase::StaticClass();
	}	
	
	WaveState = EWaveState::Spawning;

	//Start spawning enemies by timer, so they don't stack.
	GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &ATDGameMode::SpawnEnemy, WavesSettings->GetTimeBetweenSpawns(), true, 0.f);

}


void ATDGameMode::GiveReward_Implementation(AActor* Killer, AActor* Victim)
{
	UTDFLib::AddCurrency(Killer, ECurrency::Gold, 50);
}

void ATDGameMode::SpawnEnemy()
{
	// Save variables for future uses.
	const UEnemyData* EnemyData = WavesSettings->GetWaveInfo(CurrentWaveNum)->EnemyData;
	const int32 EnemiesPerWave = WavesSettings->GetWaveInfo(CurrentWaveNum)->EnemyAmount;
	
	for(const auto& It : EnemySpawnPoints)
	{		
		AEnemyBase* Enemy = GetWorld()->SpawnActorDeferred<AEnemyBase>(ClassToSpawn, It->GetActorTransform(), this, nullptr, ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn);
		if(Enemy)
		{
			// EnemyData was checked earlier in SpawnWave().
			Enemy->InitCharacter(EnemyData);	
			
			Enemy->FinishSpawning(It->GetActorTransform());

			Enemy->GetHealthComponent()->OnDeathDelegate.AddDynamic(this, &ATDGameMode::OnEnemyDeath);
			Enemy->GetHealthComponent()->OnKilledDelegate.AddDynamic(this, &ATDGameMode::GiveReward);

			if(Enemy)
			{
				EnemyAlive++;
			}
		}
	}
	LoopCount++;
	if(LoopCount >=  EnemiesPerWave)
	{
		WaveState = EWaveState::InProgress;
		
		LoopCount = 0;
		GetWorld()->GetTimerManager().ClearTimer(SpawnTimer);
	}
		
}

void ATDGameMode::OnEnemyDeath()
{
	EnemyAlive--;

	if(EnemyAlive == 0 && WaveState == EWaveState::InProgress)
	{
		EndWave();
	}
	
}


void ATDGameMode::OnCastleDestroyed()
{

	GameOver(true);
	
	Event_OnCastleDestroyed();
}

// Called by OnCastleDestroyed() and OnEnemyDeath() if it was last wave.
void ATDGameMode::GameOver(const bool IsWin)
{
	bGameOver = true;

	for(const auto& It : Players)
	{
		It->OnGameOver(IsWin);
	}
	
}

void ATDGameMode::EndWave()
{
	WaveState = EWaveState::WaitingForStart;

	Event_OnWaveEnded();
	//Check is that was last wave and isn't game infinite
	//Wave settings was checked on wave spawn
	if(CurrentWaveNum >= LastWaveNum && !WavesSettings->IsGameInfinite())
	{
		GameOver(true);
			
		return;
	}

		
	if(!bGameOver)
	{
		GetWorld()->GetTimerManager().SetTimer(SpawnTimer, this, &ATDGameMode::StartWave, WavesSettings->GetTimeBetweenWaves(), false);
	}
}


void ATDGameMode::OnPlayerReady(ATDPlayerController* Player, bool NewReady)
{
	PlayersReady[Player] = NewReady;

	for(const auto& It : PlayersReady)
	{
		if(!It.Value)
		{
			return;
		}
	}

	StartWave();
}

