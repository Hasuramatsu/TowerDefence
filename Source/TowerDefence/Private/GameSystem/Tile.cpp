// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSystem/Tile.h"
#include  "DrawDebugHelpers.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;
}


// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATile::InitTileProperty(const FTileProperty& NewProperty)
{
	TileProperty = NewProperty;
}

void ATile::DrawDebug()
{
	FVector Extent = FVector(TileProperty.TileSize);
	DrawDebugBox(GetWorld(), GetActorLocation(), Extent, FColor::Magenta,true, 3.f);

	FString String = FString::Printf(TEXT("Location: %s"), *(GetActorLocation().ToString()));
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, String);
}


