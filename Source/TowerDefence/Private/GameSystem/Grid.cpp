// Fill out your copyright notice in the Description page of Project Settings.


#include "GameSystem/Grid.h"

// Sets default values for this component's properties
AGrid::AGrid()
{

	// ...

	SpawnGrid();
}


// Called when the game starts
void AGrid::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void AGrid::DrawDebug()
{
	for(TTuple<FVector2D, ATile*>& It : TileMap)
	{
		It.Value->DrawDebug();
	}
}

void AGrid::SpawnGrid()
{
	for(int32 i = 0; i < GridProperty.GridX; i++)
	{
		for(int32 k = 0; k < GridProperty.GridY; k++)
		{
			FTransform TileTransform;
			FVector Location = GridProperty.OriginLocation + FVector(i * TileProperty.TileSize, k * TileProperty.TileSize, 0.f);
			TileTransform.SetLocation(Location);
			ATile* NewTile = Cast<ATile>(GetWorld()->SpawnActorDeferred<ATile>(TileClass, TileTransform, this, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn));
			if(NewTile)
			{
				NewTile->TileInfo.TileCoordinates = FVector2D(i, k);
				NewTile->InitTileProperty(TileProperty);

				

				NewTile->FinishSpawning(TileTransform);

				TileMap.Add(NewTile->TileInfo.TileCoordinates, NewTile);
								
				NewTile->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
			}
		}
	}
}


