// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilitySystem/TDAbilitySystemComponent.h"

void UTDAbilitySystemComponent::RegisterNewAbilities(TArray<TSubclassOf<UTDGameplayAbility>>& NewAbilities)
{
	TArray<FGameplayAbilitySpec> NewSpecs;

	for(auto& Ability : NewAbilities)
	{
		if(Ability.Get())
		{
			NewSpecs.Add(FGameplayAbilitySpec(Ability, 1, INDEX_NONE, this));
		}
	}

	for(auto& Spec : NewSpecs)
	{
		FGameplayAbilitySpecHandle& SpecHandle = OwningAbilities.FindOrAdd(Spec.Ability->GetName());
		if(!SpecHandle.IsValid())
		{
			SpecHandle = GiveAbility(Spec);
		}
	}
}

void UTDAbilitySystemComponent::ApplyEffects(TArray<TSubclassOf<UGameplayEffect>> NewEffects)
{
	for(const auto& Effect : NewEffects)
	{
		FGameplayEffectContextHandle EffectContext = MakeEffectContext();
		EffectContext.AddSourceObject(GetOwner());

		FGameplayEffectSpecHandle NewHandle = MakeOutgoingSpec(Effect, 1, EffectContext);

		if(NewHandle.IsValid())
		{
			
			FActiveGameplayEffectHandle ActiveGameplayEffectHandle = ApplyGameplayEffectSpecToTarget(*NewHandle.Data.Get(), this);
		}
	}
}

bool UTDAbilitySystemComponent::GetGameplayAbilitiesWithMatchingTags(const FGameplayTagContainer& GameplayTagContainer,
	TArray<UTDGameplayAbility*>& ActiveAbilities)
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer, AbilitiesToActivate, false);

	if(AbilitiesToActivate.Num() <= 0)
	{
		return false;
	}

	for(const auto& Ability : AbilitiesToActivate)
	{
		TArray<UGameplayAbility*> AbilityInstances = Ability->GetAbilityInstances();

		for(const auto& Instance : AbilityInstances)
		{
			ActiveAbilities.Add(Cast<UTDGameplayAbility>(Instance));
		}
	}

	
	
	return ActiveAbilities.Num() > 0;
}
