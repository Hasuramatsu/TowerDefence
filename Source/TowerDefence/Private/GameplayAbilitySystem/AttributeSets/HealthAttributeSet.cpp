// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilitySystem/AttributeSets/HealthAttributeSet.h"

#include "GameplayEffectExtension.h"
#include "TDFLib.h"
#include "GameplayAbilitySystem/TDAbilitySystemComponent.h"
#include "Net/UnrealNetwork.h"

UHealthAttributeSet::UHealthAttributeSet() : Health(100.f), MaxHealth(100.f), HealthRegeneration(0.f), Damage(0.f)
{
	
}

void UHealthAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthAttributeSet, Health);
	DOREPLIFETIME(UHealthAttributeSet, MaxHealth);
//	DOREPLIFETIME(UHealthAttributeSet, HealthRegeneration);
}

void UHealthAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);

	if(Attribute == GetMaxHealthAttribute())
	{
		AdjustAttributeForMaxChange(Health, MaxHealth, NewValue, GetHealthAttribute());
	}

	
}

void UHealthAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{
	Super::PostAttributeChange(Attribute, OldValue, NewValue);

	if(GetOwningActor()->GetLocalRole() == ROLE_Authority)
	{
		if(Attribute == GetHealthAttribute())
		{
			OnRep_Health(OldValue);
		}

		if(Attribute == GetMaxHealthAttribute())
		{
			OnRep_MaxHealth(OldValue);
		}
	}
}

void UHealthAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle ContextHandle = Data.EffectSpec.GetContext();
	UTDAbilitySystemComponent* Source = Cast<UTDAbilitySystemComponent>(ContextHandle.GetOriginalInstigatorAbilitySystemComponent());

	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	float DeltaValue = 0.f;

	if(Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
	{
		DeltaValue = Data.EvaluatedData.Magnitude;
	}

	AActor* TargetActor{nullptr};
	AController* TargetController{nullptr};
	APawn* TargetPawn{nullptr};

	if(Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetPawn = Cast<APawn>(TargetActor);
	}

	if(Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		AActor* SourceActor{nullptr};
		AController* SourceController{nullptr};
		APawn* SourcePawn{nullptr};

		if(Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
		{
			SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
			SourceController = Source->AbilityActorInfo->PlayerController.Get();

			if(SourceController == nullptr && SourceActor != nullptr)
			{
				if(APawn* Pawn = Cast<APawn>(SourceActor))
				{
					SourceController = Pawn->GetController();
				}
			}

			if(SourceController)
			{
				SourcePawn = SourceController->GetPawn();
			}
			else
			{
				SourcePawn = Cast<APawn>(SourceActor);
			}

			if(ContextHandle.GetEffectCauser())
			{
				SourceActor = ContextHandle.GetEffectCauser();
			}
		}

		FHitResult HitResult;
		if(ContextHandle.GetHitResult())
		{
			HitResult = *ContextHandle.GetHitResult();
		}

		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);

		if(LocalDamageDone)
		{
			const float OldHealth = GetHealth();
			SetHealth(FMath::Clamp(OldHealth - LocalDamageDone, 0.f, GetMaxHealth()));

			// Maybe Damage Delegates
			OnDamageTakenDelegate.Broadcast(LocalDamageDone, SourceActor, TargetActor, !UTDFLib::IsAlive(TargetActor));

		}
		
	}

}

void UHealthAttributeSet::OnRep_Health(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHealthAttributeSet, Health, OldValue);

	OnHealthChangeDelegate.Broadcast(OldValue.GetCurrentValue(), GetHealth());
}

void UHealthAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UHealthAttributeSet, MaxHealth, OldValue);

	OnMaxHealthChangeDelegate.Broadcast(OldValue.GetCurrentValue(), GetMaxHealth());
}

// void UHealthAttributeSet::OnRep_HealthRegeneration(const FGameplayAttributeData& OldValue)
// {
// 	GAMEPLAYATTRIBUTE_REPNOTIFY(UHealthAttributeSet, HealthRegeneration, OldValue);
//
// 	OnHealthRegenerationChangeDelegate.Broadcast(OldValue.GetCurrentValue(), GetHealthRegeneration());
// }
