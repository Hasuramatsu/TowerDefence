// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilitySystem/AttributeSets/DefenceAttributeSet.h"

UDefenceAttributeSet::UDefenceAttributeSet() : DamageReduction(0.f)
{
}

void UDefenceAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
}
