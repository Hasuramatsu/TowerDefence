// Fill out your copyright notice in the Description page of Project Settings.


#include "GameplayAbilitySystem/AttributeSets/OffenceAttributeSet.h"

UOffenceAttributeSet::UOffenceAttributeSet() : AttackDamage(10.f),
AttackSpeed(0.7), AttackRange(1000.f),
ProjectileSpeedMulti(0.f), AreaOfEffect(0.f)
{
	
}


void UOffenceAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void UOffenceAttributeSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	Super::PreAttributeChange(Attribute, NewValue);
}

void UOffenceAttributeSet::PostAttributeChange(const FGameplayAttribute& Attribute, float OldValue, float NewValue)
{
	Super::PostAttributeChange(Attribute, OldValue, NewValue);

	if(Attribute == GetAttackRangeAttribute())
	{
		OnAttackRangeChangedDelegate.Broadcast(OldValue, NewValue);
	}
}

void UOffenceAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);
}


