// Fill out your copyright notice in the Description page of Project Settings.


#include "MapBounds.h"

// Sets default values
AMapBounds::AMapBounds()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	RootComponent = BoxComponent;

	BoxComponent->SetBoxExtent(FVector(300.f, 300.f, 300.f));
}



// Called when the game starts or when spawned
void AMapBounds::BeginPlay()
{
	Super::BeginPlay();
	
}

 FVector AMapBounds::GetMinBounds() const
{
	return BoxComponent->Bounds.GetBox().Min;
}

 FVector AMapBounds::GetMaxBounds() const
{
	return BoxComponent->Bounds.GetBox().Max;
}