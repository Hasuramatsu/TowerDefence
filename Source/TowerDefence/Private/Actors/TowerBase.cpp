// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/TowerBase.h"

#include "Actors/TowerController.h"
#include "DataAssets/TowerData.h"
#include "Net/UnrealNetwork.h"
#include "Player/TDPlayerController.h"

// Sets default values
ATowerBase::ATowerBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneCompoent"));
	RootComponent = SceneComponent;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);

	AbilitySystemComponent = CreateDefaultSubobject<UTDAbilitySystemComponent>(TEXT("AbilitySystemComponent"));

	OffenceAttributes = CreateDefaultSubobject<UOffenceAttributeSet>(TEXT("OffenceAttributes"));
	

	AutoPossessAI = EAutoPossessAI::Spawned;

	static ConstructorHelpers::FClassFinder<ATowerController> TowContrlClass (TEXT("Blueprint'/Game/TowerDefence/Core/Towers/BP_TowerController_Base.BP_TowerController_Base_C'"));
	if(TowContrlClass.Class)
	{
		AIControllerClass = TowContrlClass.Class;
	}

	bInitialized = false;
	
}


void ATowerBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATowerBase, bInitialized);
	DOREPLIFETIME(ATowerBase, PlayerController);
}

void ATowerBase::InitTower_Implementation(const UTowerData* Property, ATDPlayerController* NewPlayerController)
{
	if(GetLocalRole() == ENetRole::ROLE_Authority)
	{
		PlayerController = NewPlayerController;	
	}
	
	if(StaticMeshComponent && Property->Mesh)
	{
		StaticMeshComponent->SetStaticMesh(Property->Mesh);
	}

	if(StaticMeshComponent->GetMaterial(0))
	{
		// Creating Dynamic Material for test purpose. TODO Remove
		UMaterialInstanceDynamic* Material = StaticMeshComponent->CreateAndSetMaterialInstanceDynamicFromMaterial(0,StaticMeshComponent->GetMaterial(0));
		if(Material)
		{
			Material->SetVectorParameterValue(TEXT("Color"), Property->Color);
		}
	}

	DisplayInfo = Property->DisplayInfo;

	if(AbilitySystemComponent)
	{
		TArray<TSubclassOf<UTDGameplayAbility>> StartingAbilities = Property->StartingAbilities;
		AbilitySystemComponent->RegisterNewAbilities(StartingAbilities);
		AbilitySystemComponent->ApplyEffects(Property->StartingEffects);
	}

	StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	bInitialized = true;
}

// Called when the game starts or when spawned
void ATowerBase::BeginPlay()
{
	Super::BeginPlay();

//	if(!bInitialized && GetLocalRole() == ROLE_Authority)
//	{
//		InitTower(FTowerProperty());
//	}
}



// Called every frame
void ATowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FString String = FString::Printf(TEXT("Tower owner: %s"), *GetOwner()->GetName());
	GEngine->AddOnScreenDebugMessage(-1, DeltaTime, FColor::Yellow, String);
}

ATDPlayerController* ATowerBase::GetPlayerController() const
{
	return PlayerController;
}

UCurrencyStorageComponent* ATowerBase::GetCurrencyStorage_Implementation()
{
	return Cast<ICurrencyStorageInterface>(PlayerController) ? Execute_GetCurrencyStorage(PlayerController) : nullptr;
}


