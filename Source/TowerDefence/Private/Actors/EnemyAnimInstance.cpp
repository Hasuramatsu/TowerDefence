// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/EnemyAnimInstance.h"


UAnimSequence* UEnemyAnimInstance::GetRandomSequenceFromArray(TArray<UAnimSequence*>& Array)
{
	return Array.Num() > 0 ? Array[FMath::RandRange(0, Array.Num() - 1)] : nullptr;
}

void UEnemyAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();

	OwnEnemy = Cast<AEnemyBase>(TryGetPawnOwner());
	if(OwnEnemy)
	{
		OwnEnemy->GetHealthComponent()->OnDeathDelegate.AddDynamic(this, &UEnemyAnimInstance::OnDead);
		
	}
}

void UEnemyAnimInstance::OnDead_Implementation()
{
	bIsAlive = false;

	OnDead_BP();
}
