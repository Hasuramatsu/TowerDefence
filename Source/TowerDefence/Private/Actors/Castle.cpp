// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/Castle.h"

// Sets default values
ACastle::ACastle()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	RootComponent = CollisionSphere;
	CollisionSphere->SetSphereRadius(400.f);
	CollisionSphere->SetCollisionProfileName("OverlapAll");
	CollisionSphere->SetSimulatePhysics(false);

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	Mesh->SetupAttachment(RootComponent);
	Mesh->SetCollisionProfileName("NoCollision");

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->OnDeathDelegate.AddDynamic(this, &ACastle::OnDeath);

	bReplicates = true;
}


// Called when the game starts or when spawned
void ACastle::BeginPlay()
{
	Super::BeginPlay();

	// FHealthProperty HealthProperty;
	// HealthProperty.MaxHealth = 100.f;
	HealthComponent->InitHealth(100.f);
	
}


void ACastle::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	
}


void ACastle::OnDeath()
{

	OnCastleDestroyed.Broadcast();
	// Call BP function
	OnDeathEvent();
}


