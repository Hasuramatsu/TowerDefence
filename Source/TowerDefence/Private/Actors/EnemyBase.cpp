// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/EnemyBase.h"

#include "AIController.h"
#include "Actors/Castle.h"
#include "Components/CapsuleComponent.h"
#include "DataAssets/EnemyData.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AEnemyBase::AEnemyBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	bUseControllerRotationYaw = false;

	AbilitySystemComponent = CreateDefaultSubobject<UTDAbilitySystemComponent>(TEXT("AbilitySystemComponent"));

	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->OnDeathDelegate.AddDynamic(this, &AEnemyBase::OnDeath);

	HealthAttributes = CreateDefaultSubobject<UHealthAttributeSet>(TEXT("HealthAttributes"));	
	DefenceAttributes = CreateDefaultSubobject<UDefenceAttributeSet>(TEXT("DefenceAttributes"));

	WidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("HealthBar"));
	WidgetComp->SetupAttachment(RootComponent);
	WidgetComp->SetRelativeLocation(FVector(0.f, 0.f, 150.f));
	WidgetComp->SetDrawSize(FVector2D(150, 10));

	//WidgetComp->SetWidget()
	static ConstructorHelpers::FClassFinder<UUserWidget> HPBar(TEXT("WidgetBlueprint'/Game/TowerDefence/UMG/WBP_HPBar.WBP_HPBar_C'"));
	
	if(TSubclassOf<UUserWidget> Bar = HPBar.Class)
	{
	//	UUserWidget* Bar = Cast<UUserWidget>(HPBar.Object->GenerateClass);
		WidgetComp->SetWidgetClass(Bar);
		WidgetComp->SetWidgetSpace(EWidgetSpace::Screen);
	}
	
	bInitialized = false;

	bReplicates = true;
	SetReplicatingMovement(true);

}

void AEnemyBase::InitCharacter(const UEnemyData* Prop)
{
	//CharProp = Prop;

	
	if(GetMesh() && Prop->EnemyMesh)
	{
		GetMesh()->SetSkeletalMesh(Prop->EnemyMesh);

		GetMesh()->SetRelativeRotation(FRotator(0, -90, 0));

		GetMesh()->SetAnimClass(Prop->AnimBlueprint);

		if(GetMesh()->GetMaterial(0))
		{
			// Creating Dynamic Material for test purpose. TODO Remove
			UMaterialInstanceDynamic* Material = GetMesh()->CreateAndSetMaterialInstanceDynamicFromMaterial(0,GetMesh()->GetMaterial(0));
			if(Material)
			{
				Material->SetVectorParameterValue(TEXT("Color"), Prop->Color);
			}
		}
	}

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->MaxWalkSpeed = Prop->BaseMoveSpeed;

	Damage = Prop->Damage;

	if(HealthComponent && HealthAttributes)
	{
		HealthAttributes->OnHealthChangeDelegate.AddDynamic(HealthComponent, &UHealthComponent::OnHealthChangeHandle);
		HealthAttributes->OnMaxHealthChangeDelegate.AddDynamic(HealthComponent, &UHealthComponent::OnMaxHealthChangeHandle);
		HealthAttributes->OnDamageTakenDelegate.AddDynamic(HealthComponent, &UHealthComponent::OnDamageTakenHandle);
	}
	HealthComponent->InitHealth(HealthAttributes->GetMaxHealth());

	if(AbilitySystemComponent)
	{
		TArray<TSubclassOf<UTDGameplayAbility>> Abilities = Prop->StartingAbilities;
		AbilitySystemComponent->RegisterNewAbilities(Abilities);
		AbilitySystemComponent->ApplyEffects(Prop->StartingEffects);
	}

	bInitialized = true;
}

// Called when the game starts or when spawned
void AEnemyBase::BeginPlay()
{
	Super::BeginPlay();

	if(AAIController* AIController = Cast<AAIController>(GetController()))
	{
		if(AActor* Castle = UGameplayStatics::GetActorOfClass(GetWorld(), ACastle::StaticClass()))
		{
			AIController->MoveToActor(Castle);
		}
		
	}
	
	
	//TODO Remove
	//if(!bInitialized)
	//{
	//	InitCharacter(CharProp);
	//}

	UHealthBar* HealthBar = Cast<UHealthBar>(WidgetComp->GetWidget());
	
	if(HealthBar)
	{
		HealthBar->InitHealthBar(this, false);
	}
}

void AEnemyBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	//DOREPLIFETIME(AEnemyBase, CharProp);
}

void AEnemyBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	ACastle* Castle = Cast<ACastle>(OtherActor);
	if(Castle)
	{
		UGameplayStatics::ApplyDamage(Castle, Damage, GetController(), this, UDamageType::StaticClass());

		GetHealthComponent()->OnDeathDelegate.Broadcast();
		Destroy(true);
	}
}


void AEnemyBase::OnDeath()
{
	GetCharacterMovement()->StopMovementImmediately();

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	WidgetComp->SetHiddenInGame(true);
	//Destroy(true);
	//Call BP Event
	OnDeathEvent();
}


