// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/TowerDummy.h"

#include "Kismet/KismetSystemLibrary.h"
#include "Player/TDPlayerController.h"

// Sets default values
ATowerDummy::ATowerDummy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneCompoennt"));
	RootComponent = SceneComponent;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);
	//StaticMeshComponent->SetRelativeLocation(FVector(0.f, 0.f, 150));
	StaticMeshComponent->SetCollisionProfileName(TEXT("NoCollision"));

	static ConstructorHelpers::FObjectFinder<UMaterial> MatFillObj(TEXT("Material'/Game/TowerDefence/MaterialLibrary/M_Dummy.M_Dummy'"));

	Material = MatFillObj.Object;

	GridSize = 5;
	ObjectSize = 150.f;

	bValidLocation = false;
}

void ATowerDummy::InitDummy(UStaticMesh* Mesh, FString NewTowerID, ATDPlayerController* Player)
{
	if(StaticMeshComponent)
	{
		StaticMeshComponent->SetStaticMesh(Mesh);
		FVector MeshSize = StaticMeshComponent->GetStaticMesh()->GetBoundingBox().GetSize();
		ObjectSize = FMath::Max(MeshSize.X, MeshSize.Y);
	}

	TowerID = NewTowerID;
	
	SetOwningPlayer(Player);
}

// Called when the game starts or when spawned
void ATowerDummy::BeginPlay()
{
	Super::BeginPlay();

	if(Material)
	{
		DynamicMaterial = UMaterialInstanceDynamic::Create(Material, this);

		if(StaticMeshComponent)
		{
			int32 MatNums = StaticMeshComponent->GetNumMaterials();

			for(int32 i = 0; i < MatNums; i++)
			{
				StaticMeshComponent->SetMaterial(i, DynamicMaterial);
			}
		}
	}

}

// Called every frame
void ATowerDummy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(OwningPlayer)
	{
		FHitResult HitResult;
		OwningPlayer->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, false, HitResult);

		if(HitResult.IsValidBlockingHit())
		{
			SetActorLocation(HitResult.Location);
		}
	}

	bValidLocation = IsValidPlace();

	switch (bValidLocation)
	{
	case true:
		
		DynamicMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Green);

		
		break;
	case false:

		DynamicMaterial->SetVectorParameterValue(TEXT("Color"), FLinearColor::Red);

		
		break;
	}
	
	
}

bool ATowerDummy::IsValidPlace()
{
	float TileSize = ObjectSize / GridSize;
	
	const float Offset = ObjectSize / 2 + TileSize;

	TileSize = ObjectSize / GridSize;

	
	FVector GridStart = GetActorLocation() - FVector(Offset, Offset, 0.f);

	FVector FirstTileLocation = GridStart - FVector(TileSize, TileSize, 0.f);

	for(int32 i = 0; i <= GridSize + 4; i++)
	{
		for(int32 k = 0; k <= GridSize + 4; k++)
		{
			FVector TempLocation = FirstTileLocation + FVector(TileSize * i, TileSize * k, 0.f);

			TArray<AActor*> ActorToIgnore;

			FHitResult HitResult;
			
			UKismetSystemLibrary::BoxTraceSingleByProfile(GetWorld(), TempLocation, TempLocation - FVector(0.f, 0.f, 10),
				FVector(TileSize/2), FRotator::ZeroRotator, TEXT("BlockOnlyBuild"), false, ActorToIgnore,
				EDrawDebugTrace::ForOneFrame, HitResult, true);

			if(!HitResult.bBlockingHit)
			{
				return false;
			}
			
			UKismetSystemLibrary::BoxTraceSingleByProfile(GetWorld(), TempLocation, TempLocation - FVector(0.f, 0.f, 10),
				FVector(TileSize/2), FRotator::ZeroRotator, TEXT("IgnoreStatic"), false, ActorToIgnore,
				EDrawDebugTrace::ForOneFrame, HitResult, true);
			
			if(HitResult.bBlockingHit)
			{
				return false;
			}
		}
	}

	return true;
}

