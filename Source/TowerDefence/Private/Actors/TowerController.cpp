// Fill out your copyright notice in the Description page of Project Settings.


#include "Actors/TowerController.h"

#include "Actors/TowerBase.h"
#include "BehaviorTree/BlackboardComponent.h"

ATowerController::ATowerController()
{
}

void ATowerController::OnAttackRangeChangedHandle(float OldValue, float NewValue)
{
	if(Blackboard)
	{
		Blackboard->SetValueAsFloat(TEXT("AttackRange"), NewValue);
	}
}


void ATowerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if(GetLocalRole() == ROLE_Authority)
	{
		if(BehaviorTree)
		{
			RunBehaviorTree(BehaviorTree);
		}

		PossesedTower = Cast<ATowerBase>(InPawn);
		if(PossesedTower)
		{
			const float NewAttackRange = PossesedTower->GetOffenceAttributeSet()->GetAttackRange();
			OnAttackRangeChangedHandle(NewAttackRange, NewAttackRange);
		
			PossesedTower->GetOffenceAttributeSet()->OnAttackRangeChangedDelegate.AddDynamic(this, &ATowerController::OnAttackRangeChangedHandle);
		}
	}
}

void ATowerController::OnUnPossess()
{
	Super::OnUnPossess();

	if(PossesedTower)
	{
		PossesedTower->GetOffenceAttributeSet()->OnAttackRangeChangedDelegate.RemoveAll(this);
	}
}
