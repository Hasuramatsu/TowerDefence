// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/HealthComponent.h"

#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	SetIsReplicatedByDefault(true);

	bAlive = true;

	//OnCurrentHealthChangeDelegate.AddDynamic(this, &UHealthComponent::OnCurrentHealthChangeFunc);
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

//	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnOwnerTakeDamage);
	
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, CurrentHealth);
	DOREPLIFETIME(UHealthComponent, MaxHealth);
}

void UHealthComponent::OnRep_CurrentHealth(float OldValue)
{

	OnCurrentHealthChangeDelegate.Broadcast(OldValue, CurrentHealth);
	
	if(FMath::IsNearlyZero(CurrentHealth) && bAlive)
	{
		bAlive = false;
		OnDeathDelegate.Broadcast();
	}
}

// void UHealthComponent::OnOwnerTakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
// 	AController* InstigatedBy, AActor* DamageCauser)
// {
// 	if(Damage > 0)
// 	{
// 		TakeDamage(Damage);
// 	}
// 	else if(Damage < 0)
// 	{
// 		TakeHeal(Damage);
// 	}
// }


void UHealthComponent::InitHealth(float NewMaxHealth)
{
	MaxHealth = NewMaxHealth;
	CurrentHealth = MaxHealth;
}


void UHealthComponent::SetCurrentHealth_Implementation(float NewValue)
{
	const float TempHealth = CurrentHealth;
	CurrentHealth = FMath::Clamp(NewValue, 0.f, MaxHealth);
	
	OnRep_CurrentHealth(TempHealth);

}

void UHealthComponent::SetMaxHealth_Implementation(float NewValue)
{
	const float TempMaxHealth = MaxHealth;
	MaxHealth = FMath::Max(0.f, NewValue);

	if(TempMaxHealth != MaxHealth)
	{
		OnMaxHealthChangeDelegate.Broadcast(TempMaxHealth, MaxHealth);
		
		float Multiplier = TempMaxHealth / MaxHealth;
		SetCurrentHealth(CurrentHealth * Multiplier);
	}
}

// float UHealthComponent::TakeDamage(const float DamageAmount)
// {
// 	if(FMath::IsNearlyZero(CurrentHealth))
// 	{
// 		return 0.f;
// 	}
// 	
// 	const float TempHealth = CurrentHealth;
// 	CurrentHealth = FMath::Clamp(CurrentHealth - DamageAmount, 0.f, MaxHealth);
// 	OnRep_CurrentHealth(TempHealth);
// 	// if(TempHealth != CurrentHealth)
// 	// {
// 	// 	OnCurrentHealthChangeDelegate.Broadcast(TempHealth, CurrentHealth);
// 	// }
//
// 	return FMath::Max(TempHealth - CurrentHealth, 0.f);
// }
//
// float UHealthComponent::TakeHeal(const float HealAmount)
// {
// 	if(FMath::IsNearlyZero(CurrentHealth))
// 	{
// 		return 0.f;
// 	}
// 	
// 	const float TempHealth = CurrentHealth;
// 	CurrentHealth = FMath::Clamp(CurrentHealth + HealAmount, 0.f, MaxHealth);
// 	OnRep_CurrentHealth(TempHealth);
// 	// if(TempHealth != CurrentHealth)
// 	// {
// 	// 	OnCurrentHealthChangeDelegate.Broadcast(TempHealth, CurrentHealth);
// 	// }
//
// 	return FMath::Max(CurrentHealth - TempHealth, 0.f);
// }

//
// void UHealthComponent::OnCurrentHealthChangeFunc_Implementation(float OldValue, float NewValue)
// {
//
// }

void UHealthComponent::OnHealthChangeHandle(float OldValue, float NewValue)
{
	SetCurrentHealth(NewValue);
}

void UHealthComponent::OnMaxHealthChangeHandle(float OldValue, float NewValue)
{
	SetMaxHealth(NewValue);
}

void UHealthComponent::OnDamageTakenHandle(float DamageTaken, AActor* DamageDealer, AActor* DamageReceiver,
	bool bBIsKillingBlow)
{
	if(bBIsKillingBlow)
	{
		OnKilledDelegate.Broadcast(DamageDealer, GetOwner());
	}
}


