// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CurrencyStorageComponent.h"

#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UCurrencyStorageComponent::UCurrencyStorageComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCurrencyStorageComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UCurrencyStorageComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UCurrencyStorageComponent, Currencies);
}

void UCurrencyStorageComponent::Server_AddNewCurrencyType_Implementation(const ECurrency NewCurrency, int32 BaseValue)
{
	Currencies.Add(NewCurrency, BaseValue);

	OnCurrencyTypeAdded.Broadcast(NewCurrency, BaseValue);
}

void UCurrencyStorageComponent::Server_AddCurrency_Implementation(const ECurrency Currency, int32 Amount)
{
	if(!Currencies.Contains(Currency)) return;

	// Sure we don't pass negative number.
	Amount = FMath::Max(0, Amount);
	int32 OldValue = Currencies[Currency];
	//Currencies.Add(Currency, Amount);
	Currencies[Currency] += Amount;

	OnCurrencyValueChanged.Broadcast(Currency, OldValue, Currencies[Currency]);
}

void UCurrencyStorageComponent::Server_WithdrawCurrency_Implementation(const ECurrency Currency, int32 Amount)
{
	if(!Currencies.Contains(Currency)) return;

	// Sure we don't pass negative number.
	Amount = FMath::Max(0, Amount);
	int32 OldValue = Currencies[Currency];
	Currencies[Currency] -= Amount;

	OnCurrencyValueChanged.Broadcast(Currency, OldValue, Currencies[Currency]);
}

bool UCurrencyStorageComponent::IsEnoughCurrency(const ECurrency Currency, int32 NeededAmount) const
{
	if(Currencies.Find(Currency) < 0) return false;
	
	return Currencies[Currency] >= NeededAmount;
}

int32 UCurrencyStorageComponent::GetCurrencyAmount(const ECurrency Currency) const
{
	return Currencies.Contains(Currency) ? Currencies[Currency] : 0;
}

int32 UCurrencyStorageComponent::GetAmount(const ECurrency Currency) const
{
	if(!Currencies.Contains(Currency)) return -1;

	return Currencies[Currency];
}

