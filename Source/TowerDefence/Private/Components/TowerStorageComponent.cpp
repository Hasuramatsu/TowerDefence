// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TowerStorageComponent.h"

// Sets default values for this component's properties
UTowerStorageComponent::UTowerStorageComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UTowerStorageComponent::BeginPlay()
{
	Super::BeginPlay();

	// Adding starting towers in storage;
	for(const auto& It : StartingTowers)
	{
		Towers.Add(It->TowerID, It);
	}

	StartingTowers.Empty();
}

UTowerData* UTowerStorageComponent::GetTower(FString TowerID)
{
	return Towers.FindRef(TowerID);
}

void UTowerStorageComponent::GetTowers(TArray<UTowerData*>& OutTowers) const
{
	Towers.GenerateValueArray(OutTowers);
}

FString UTowerStorageComponent::AddTower(UTowerData* TowerData)
{
	Towers.FindOrAdd(TowerData->TowerID, TowerData);

	return TowerData->TowerID;
}




